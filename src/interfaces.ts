// See https://github.com/codeclimate/platform/blob/master/spec/analyzers/SPEC.md#data-types

export interface Issue {
	type: 'issue';
	check_name: string;
	description: string;
	content?: string;
	categories: string[];
	location: Location;
	severity: Severity;
}

interface Location {
	path: string;
	lines?: {
		begin: number;
		end: number;
	};
	positions?: {
		begin: { line: number; column: number; };
		end: { line: number; column: number; };
	};
}

type Severity = 'info' | 'minor' | 'major' | 'critical' | 'blocker';
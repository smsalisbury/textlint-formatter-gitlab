import { TextlintResult } from '@textlint/types';

import { Issue } from './interfaces';

function formatForGitlab(results: TextlintResult[]): string {
	return JSON.stringify(resultsToIssues(results));
}

function resultsToIssues(results: TextlintResult[]): Issue[] {
	return results.flatMap(file =>
		file.messages.map(message => ({
			type: 'issue',
			check_name: message.ruleId,
			description: message.message,
			categories: ['Style'],
			severity: message.severity === 2 ? 'major' : 'minor',
			location: {
				path: file.filePath,
				positions: {
					begin: message.loc.start,
					end: message.loc.end,
				}
			}
		})),
	);
}

export default formatForGitlab;
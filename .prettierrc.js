module.exports = {
	printWidth: 120,
	useTabs: true,
	singleQuote: true,
	arrowParens: 'avoid',
	trailingComma: 'all',
};
  
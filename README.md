[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![npm version][npm-version-shield]][npm-link]
[![npm downloads][npm-downloads-shield]][npm-link]
[![npm size][npm-size-shield]][npm-link]

<!-- PROJECT LOGO -->
# Textlint Formatter for GitLab

<!-- ABOUT THE PROJECT -->
## About The Project

Use this package to format [textlint](https://github.com/textlint/textlint) output so [GitLab can read it](https://docs.gitlab.com/ee/ci/testing/code_quality.html#implement-a-custom-tool) as a code quality report.


<!-- GETTING STARTED -->
## Getting Started

### Installation

```bash
npm i -D textlint-formatter-gitlab
```

<!-- USAGE EXAMPLES -->
## Usage

In the command line:

```bash
npx textlint -f gitlab README.md
```

## Contributing

Any contributions you make are greatly appreciated.

If you have a suggestion that would make this better, please fork the repository and create a pull request. You can also open an issue with the tag "enhancement".

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## License

This package is distributed under the MIT License. See `LICENSE.txt` for more information.

## Contact

Project Link: [https://gitlab.com/smsalisbury/textlint-formatter-gitlab](https://gitlab.com/smsalisbury/textlint-formatter-gitlab)


<!-- MARKDOWN LINKS & IMAGES -->
[contributors-shield]: https://img.shields.io/gitlab/contributors/smsalisbury/textlint-formatter-gitlab.svg?style=flat-square&logo=gitlab
[contributors-url]: https://gitlab.com/smsalisbury/textlint-formatter-gitlab/graphs/contributors
[forks-shield]: https://img.shields.io/gitlab/forks/smsalisbury/textlint-formatter-gitlab.svg?style=flat-square&logo=gitlab
[forks-url]: https://gitlab.com/smsalisbury/textlint-formatter-gitlab/network/members
[stars-shield]: https://img.shields.io/gitlab/stars/smsalisbury/textlint-formatter-gitlab.svg?style=flat-square&logo=gitlab
[stars-url]: https://gitlab.com/smsalisbury/textlint-formatter-gitlab/stargazers
[issues-shield]: https://img.shields.io/gitlab/issues/all/smsalisbury/textlint-formatter-gitlab.svg?style=flat-square&logo=gitlab
[issues-url]: https://gitlab.com/smsalisbury/textlint-formatter-gitlab/issues
[license-shield]: https://img.shields.io/gitlab/license/smsalisbury/textlint-formatter-gitlab.svg?style=flat-square
[license-url]: https://gitlab.com/smsalisbury/textlint-formatter-gitlab/blob/master/LICENSE.txt
[npm-version-shield]: https://img.shields.io/npm/v/textlint-formatter-gitlab?style=flat-square&logo=npm
[npm-size-shield]: https://img.shields.io/bundlephobia/minzip/textlint-formatter-gitlab?style=flat-square&logo=npm
[npm-downloads-shield]: https://img.shields.io/npm/dt/textlint-formatter-gitlab?style=flat-square&logo=npm
[npm-link]: https://www.npmjs.com/package/textlint-formatter-gitlab

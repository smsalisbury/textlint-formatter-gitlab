import { TextlintResult } from '@textlint/types';
import formatForGitLab from '../src/index';
import { Issue } from '../src/interfaces';

it('formats correctly', () => {
	const textlint: TextlintResult[] = [
		{
			filePath: './myfile.md',
			messages: [
				{
					type: 'lint',
					ruleId: 'semi',
					line: 1,
					column: 23,
					message: 'Expected a semicolon.',
					index: 0,
					loc: {
						start: { line: 1, column: 30 },
						end: { line: 2, column: 4 },
					},
					range: [1,1],
					severity: 2,
				}
			]
		},
	];
	const result = formatForGitLab(textlint);

	expect(result).toEqual(expect.any(String));
	const json = JSON.parse(result) as Issue[];
	expect(json.length).toBe(1);
	expect(json[0].type).toBe('issue');
	expect(json[0].check_name).toBe('semi');
	expect(json[0].description).toBe('Expected a semicolon.');
	expect(json[0].categories).toContain('Style');
	expect(json[0].severity).toBe('major');
	expect(json[0].location.path).toBe('./myfile.md');
	expect(json[0].location.positions?.begin.line).toBe(1);
	expect(json[0].location.positions?.begin.column).toBe(30);
	expect(json[0].location.positions?.end.line).toBe(2);
	expect(json[0].location.positions?.end.column).toBe(4);
});